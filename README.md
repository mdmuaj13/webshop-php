# Webshop

This is a php based web application. This application followed incremental process in software development. This is a simple e-commerce application with raw php coding. 

Through this app any user can view products, search. To buy any product they need to be registered user. This application also contains an admin panel..
Admin panel can handle most of activities of e-commerce functionalities. 

This project was created for Software Engineering Course (CSE 411).

## EWU